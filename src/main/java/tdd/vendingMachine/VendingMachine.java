package tdd.vendingMachine;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tdd.vendingMachine.model.Coin;
import tdd.vendingMachine.model.Product;
import tdd.vendingMachine.model.Shelf;
import tdd.vendingMachine.port.ChangeTray;
import tdd.vendingMachine.port.Display;
import tdd.vendingMachine.port.ProductTray;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class VendingMachine {

    @Getter
    private final Map<Integer, Shelf> shelves;

    private final Display display;

    private final List<Coin> insertedCoins = new ArrayList<>();

    private final ProductTray productTray;

    private final ChangeTray changeTray;

    private Shelf selectedShelf;

    private final Map<Coin, Integer> balance = new EnumMap<>(Coin.class);

    public void enterShelfNumber(int number) {
        Shelf shelf = shelves.get(number);
        this.selectedShelf = shelf;
        display.print("Price: ".concat(shelf.getProductTypePrice().toString()));
    }

    public void insertCoin(Coin coin) {
        BigDecimal selectedProductPrice = selectedShelf.getProductTypePrice();
        insertedCoins.add(coin);
        BigDecimal sumOfInsertedCoins = sumInsertedCoins();
        selectedProductPrice = selectedProductPrice.subtract(sumOfInsertedCoins);
        if (priceIsCovered(selectedProductPrice)) {
            BigDecimal change = BigDecimal.ZERO.subtract(selectedProductPrice);
            if (hasEnoughMoney(change)) {
                display.print("0.00");
                giveProduct(selectedShelf.getProducts().poll());
                addMoney(insertedCoins);
                insertedCoins.clear();
                giveChange(change);
            }
        } else {
            display.print(selectedProductPrice.toString());
        }
    }

    public void addMoney(List<Coin> coins) {
        coins.forEach(coin -> this.balance.compute(coin, (k, v) -> {
            if (v == null) return 1;
            else return v+1;
        }));
    }

    private boolean hasEnoughMoney(BigDecimal change) {
        BigDecimal totalBalanceSum = balance.entrySet().stream()
            .map(e -> e.getKey().getValue().multiply(new BigDecimal(e.getValue())))
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        if (change.compareTo(totalBalanceSum) > 0) {
            display.print("There is not enough money to give change");
            cancel();
            return false;
        }
        return true;
    }

    public void cancel() {
        changeTray.giveChange(new ArrayList<>(insertedCoins));
        insertedCoins.clear();
    }

    private BigDecimal sumInsertedCoins() {
        return insertedCoins.stream()
            .map(Coin::getValue)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private boolean priceIsCovered(BigDecimal selectedProductPrice) {
        return selectedProductPrice.compareTo(BigDecimal.ZERO) < 0;
    }

    private void giveProduct(Product product) {
        productTray.giveProduct(product);
    }

    private void giveChange(BigDecimal change) {
        List<Coin> result = new ArrayList<>();

        List<Coin> saldoCoins = balance.keySet().stream()
            .sorted(Comparator.comparing(Coin::getValue).reversed())
            .collect(Collectors.toList());

        while (change.compareTo(BigDecimal.ZERO) > 0) {
            Optional<Coin> x = Optional.empty();
            for (Coin coin : saldoCoins) {
                if (checkCoin(change, coin, balance)) {
                    result.add(coin);
                    change = change.subtract(coin.getValue());
                    balance.computeIfPresent(coin, (k,v) -> v-1);
                    x = Optional.of(coin);
                    break;
                }
            }
            if (x.isEmpty()) {
                cancel();
                return;
            }
        }
        changeTray.giveChange(result);
    }

    private boolean exists(Map<Coin, Integer> map, Coin coin) {
        return map.getOrDefault(coin, 0) > 0;
    }

    private boolean checkCoin(BigDecimal change, Coin coin, Map<Coin, Integer> map) {
        return coin.getValue().compareTo(change) <= 0 && exists(map, coin);
    }
}
