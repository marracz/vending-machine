package tdd.vendingMachine.model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class ProductType {

    private final String name;
    private final BigDecimal price;

    public ProductType(String name, BigDecimal price) {
        this.name = name;
        this.price = price.setScale(2, RoundingMode.DOWN);
    }
}
