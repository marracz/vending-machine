package tdd.vendingMachine.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Queue;

@Data
public class Shelf {

    private final Queue<Product> products;

    public Shelf(Queue<Product> products) throws WrongProductTypeException {
        validateAllProductsOfSameType(products);
        this.products = products;
    }

    private void validateAllProductsOfSameType(Queue<Product> products) throws WrongProductTypeException {
        long differentTypes = products.stream()
            .map(Product::getType)
            .distinct()
            .count();
        if (differentTypes > 1) {
            throw new WrongProductTypeException("There can be only products of one type on single shelf");
        }
    }

    public BigDecimal getProductTypePrice() {
        return products.stream().findFirst()
            .map(Product::getType)
            .map(ProductType::getPrice)
            .orElse(BigDecimal.ZERO);
    }
}
