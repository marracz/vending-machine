package tdd.vendingMachine.model;

public class WrongProductTypeException extends Exception {

    public WrongProductTypeException(String message) {
        super(message);
    }
}
