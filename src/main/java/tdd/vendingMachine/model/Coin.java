package tdd.vendingMachine.model;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Coin {
    COIN_5_00(BigDecimal.valueOf(5)),
    COIN_2_00(BigDecimal.valueOf(2)),
    COIN_1_00(BigDecimal.valueOf(1)),
    COIN_0_50(BigDecimal.valueOf(0.5)),
    COIN_0_20(BigDecimal.valueOf(0.2)),
    COIN_0_10(BigDecimal.valueOf(0.1));

    @Getter
    private final BigDecimal value;

    Coin(BigDecimal value) {
        this.value = value.setScale(2, RoundingMode.DOWN);
    }
}
