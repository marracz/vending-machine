package tdd.vendingMachine.port;

import tdd.vendingMachine.model.Coin;

import java.util.List;

public interface ChangeTray {

    void giveChange(List<Coin> change);
}
