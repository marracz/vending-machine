package tdd.vendingMachine.port;

import tdd.vendingMachine.model.Product;

public interface ProductTray {

    void giveProduct(Product product);
}
