package tdd.vendingMachine.port;

public interface Display {

    void print(String message);
}
