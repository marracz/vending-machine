package tdd.infrastructure.display;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import tdd.vendingMachine.port.Display;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DisplayFactory {

    public static Display display() {
        return new ConsoleDisplay();
    }
}
