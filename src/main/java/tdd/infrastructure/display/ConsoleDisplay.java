package tdd.infrastructure.display;

import tdd.vendingMachine.port.Display;

class ConsoleDisplay implements Display {

    @Override
    public void print(String message) {
        System.out.println(message);
    }
}
