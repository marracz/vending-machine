package tdd.infrastructure.changetray;

import tdd.vendingMachine.model.Coin;
import tdd.vendingMachine.port.ChangeTray;

import java.util.List;

public class ConsoleChangeTray implements ChangeTray {

    @Override
    public void giveChange(List<Coin> change) {
        System.out.println("Please take your change: " + change);
    }
}
