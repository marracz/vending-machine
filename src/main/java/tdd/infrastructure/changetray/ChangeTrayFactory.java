package tdd.infrastructure.changetray;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import tdd.vendingMachine.port.ChangeTray;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ChangeTrayFactory {

    public static ChangeTray tray() {
        return new ConsoleChangeTray();
    }

}
