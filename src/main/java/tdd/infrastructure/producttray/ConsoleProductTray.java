package tdd.infrastructure.producttray;

import tdd.vendingMachine.model.Product;
import tdd.vendingMachine.port.ProductTray;

public class ConsoleProductTray implements ProductTray {

    @Override
    public void giveProduct(Product product) {
        System.out.println("Product " + product.getType() + " ready for pick up");
    }
}
