package tdd.infrastructure.producttray;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import tdd.vendingMachine.port.ProductTray;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductTrayFactory {

    public static ProductTray tray() {
        return new ConsoleProductTray();
    }

}
