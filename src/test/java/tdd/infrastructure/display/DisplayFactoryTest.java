package tdd.infrastructure.display;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DisplayFactoryTest {


    @Test
    public void should_create_console_display() {

        assertThat(DisplayFactory.display()).isInstanceOf(ConsoleDisplay.class);
    }
}
