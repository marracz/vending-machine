package tdd.infrastructure.display;

import org.junit.Rule;
import org.junit.Test;
import tdd.vendingMachine.port.Display;
import uk.org.webcompere.systemstubs.rules.SystemOutRule;

import static org.assertj.core.api.Assertions.assertThat;


public class ConsoleDisplayTest {

    @Rule
    public SystemOutRule systemOutRule = new SystemOutRule();

    @Test
    public void should_print_message() {
        //given
        Display sut = new ConsoleDisplay();
        //when
        sut.print("Wiadomość na wyświetlacz");
        //then
        assertThat(systemOutRule.getLines()).contains("Wiadomość na wyświetlacz");
    }

}
