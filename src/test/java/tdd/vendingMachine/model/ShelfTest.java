package tdd.vendingMachine.model;


import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

import static org.assertj.core.api.Assertions.assertThat;

public class ShelfTest {


    @Test
    public void should_accept_products_of_same_type() throws WrongProductTypeException {
        //given
        Queue<Product> products = new ArrayDeque(List.of(
            new Product(new ProductType("typeA", BigDecimal.ONE)),
            new Product(new ProductType("typeA", BigDecimal.ONE))
        ));

        //when
        Shelf sut = new Shelf(products);

        //then
        assertThat(sut).isInstanceOf(Shelf.class);
    }

    @Test(expected = WrongProductTypeException.class)
    public void should_not_accept_products_of_different_types() throws WrongProductTypeException {
        //given
        Queue<Product> products = new ArrayDeque<>(List.of(
            new Product(new ProductType("typeA", BigDecimal.ONE)),
            new Product(new ProductType("typeB", BigDecimal.ONE))
        ));

        //when
        Shelf sut = new Shelf(products);

        //then
    }
}
