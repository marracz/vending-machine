package tdd.vendingMachine.model;


import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductTest {

    @Test
    public void product_should_have_type() {
        //given
        Product sut = new Product(new ProductType("Cola drink 0.25L", BigDecimal.ONE));
        //when

        //then
        assertThat(sut.getType()).isNotNull();
        assertThat(sut.getType()).isEqualTo(new ProductType("Cola drink 0.25L", BigDecimal.ONE));
    }

}
