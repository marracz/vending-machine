package tdd.vendingMachine;

import org.junit.Test;
import tdd.infrastructure.changetray.ChangeTrayFactory;
import tdd.infrastructure.display.DisplayFactory;
import tdd.infrastructure.producttray.ProductTrayFactory;
import tdd.vendingMachine.model.*;
import tdd.vendingMachine.port.ChangeTray;
import tdd.vendingMachine.port.Display;
import tdd.vendingMachine.port.ProductTray;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class VendingMachineTest {

    @Test
    public void vending_machine_contains_products() throws WrongProductTypeException {

        // given
        Queue<Product> productsShelf1 = new ArrayDeque<>(List.of(
            new Product(new ProductType("Mineral water 0.33L", BigDecimal.ONE)),
            new Product(new ProductType("Mineral water 0.33L", BigDecimal.ONE))));
        Queue<Product> productsShelf2 = new ArrayDeque<>(List.of(
            new Product(new ProductType("Chocolate bar", BigDecimal.valueOf(2)))));
        Map<Integer, Shelf> shelves = Map.of(1, new Shelf(productsShelf1), 2, new Shelf(productsShelf2));
        VendingMachine vendingMachine = new VendingMachine(shelves, DisplayFactory.display(), ProductTrayFactory.tray(),
            ChangeTrayFactory.tray());

        // then
        Map<Integer, Shelf> result = vendingMachine.getShelves();
        assertThat(result).isNotNull();
        assertThat(result).hasSize(2);
        assertThat(result.values().stream()
            .map(Shelf::getProducts)
            .flatMap(Collection::stream)
            .collect(Collectors.toList())).hasSize(3);
        assertThat(result.values().stream()
            .map(Shelf::getProducts)
            .flatMap(Collection::stream)
            .collect(Collectors.toList())).containsExactlyInAnyOrderElementsOf(
                Stream.concat(
                    productsShelf1.stream(), productsShelf2.stream()
                ).collect(Collectors.toList())
            );
    }

    @Test
    public void should_display_product_price_when_shelf_number_was_entered() throws WrongProductTypeException{

        // given
        Queue<Product> productsShelf1 = new ArrayDeque<>(List.of(
            new Product(new ProductType("Mineral water 0.33L", BigDecimal.ONE)),
            new Product(new ProductType("Mineral water 0.33L", BigDecimal.ONE))));
        Queue<Product> productsShelf2 = new ArrayDeque<>(List.of(
            new Product(new ProductType("Chocolate bar", BigDecimal.valueOf(2)))));
        Map<Integer, Shelf> shelves = Map.of(1, new Shelf(productsShelf1), 2, new Shelf(productsShelf2));
        Display displayMock = mock(Display.class);
        ProductTray productTrayMock = mock(ProductTray.class);
        ChangeTray changeTrayMock = mock(ChangeTray.class);
        VendingMachine vendingMachine = new VendingMachine(shelves, displayMock, productTrayMock, changeTrayMock);

        // when
        vendingMachine.enterShelfNumber(2);

        // then
        verify(displayMock).print("Price: 2.00");
    }

    @Test
    public void should_accept_coins_give_product_and_change() throws WrongProductTypeException {
        //given
        Queue<Product> products = new ArrayDeque<>(List.of(
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20)))));
        Map<Integer, Shelf> shelves = Map.of(1, new Shelf(products));
        Display displayMock = mock(Display.class);
        ProductTray productTrayMock = mock(ProductTray.class);
        ChangeTray changeTrayMock = mock(ChangeTray.class);
        VendingMachine sut = new VendingMachine(shelves, displayMock, productTrayMock, changeTrayMock);
        sut.addMoney(List.of(Coin.COIN_1_00, Coin.COIN_0_50, Coin.COIN_0_20, Coin.COIN_0_10));

        //when
        sut.enterShelfNumber(1);
        sut.insertCoin(Coin.COIN_2_00);
        sut.insertCoin(Coin.COIN_1_00);
        sut.insertCoin(Coin.COIN_0_50);

        //then
        verify(displayMock).print("Price: 3.20");
        verify(displayMock).print("1.20");
        verify(displayMock).print("0.20");
        verify(displayMock).print("0.00");
        verify(productTrayMock).giveProduct(new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))));
        verify(changeTrayMock).giveChange(List.of(Coin.COIN_0_20, Coin.COIN_0_10));
        verifyNoMoreInteractions(displayMock);
        verifyNoMoreInteractions(productTrayMock);
        verifyNoMoreInteractions(changeTrayMock);
    }

    @Test
    public void should_return_inserted_money_on_cancel() throws WrongProductTypeException {
        //given
        Queue<Product> products = new ArrayDeque<>(List.of(
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20)))));
        Map<Integer, Shelf> shelves = Map.of(1, new Shelf(products));
        Display displayMock = mock(Display.class);
        ProductTray productTrayMock = mock(ProductTray.class);
        ChangeTray changeTrayMock = mock(ChangeTray.class);
        VendingMachine sut = new VendingMachine(shelves, displayMock, productTrayMock, changeTrayMock);

        //when
        sut.enterShelfNumber(1);
        sut.insertCoin(Coin.COIN_0_10);
        sut.insertCoin(Coin.COIN_2_00);
        sut.insertCoin(Coin.COIN_0_50);
        sut.cancel();

        //then
        verify(changeTrayMock).giveChange(List.of(Coin.COIN_0_10, Coin.COIN_2_00, Coin.COIN_0_50));
        verifyNoMoreInteractions(changeTrayMock);
    }

    @Test
    public void should_not_give_product_and_return_money_on_insufficient_balance() throws WrongProductTypeException {
        //given
        Queue<Product> products = new ArrayDeque<>(List.of(
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20))),
            new Product(new ProductType("Princepolo", BigDecimal.valueOf(3.20)))));
        Map<Integer, Shelf> shelves = Map.of(1, new Shelf(products));
        Display displayMock = mock(Display.class);
        ProductTray productTrayMock = mock(ProductTray.class);
        ChangeTray changeTrayMock = mock(ChangeTray.class);
        VendingMachine sut = new VendingMachine(shelves, displayMock, productTrayMock, changeTrayMock);

        //when
        sut.enterShelfNumber(1);
        sut.insertCoin(Coin.COIN_2_00);
        sut.insertCoin(Coin.COIN_1_00);
        sut.insertCoin(Coin.COIN_1_00);

        //then
        verify(displayMock).print("Price: 3.20");
        verify(displayMock).print("1.20");
        verify(displayMock).print("0.20");
        verify(displayMock).print("There is not enough money to give change");
        verifyZeroInteractions(productTrayMock);
        verify(changeTrayMock).giveChange(List.of(Coin.COIN_2_00, Coin.COIN_1_00, Coin.COIN_1_00));
        verifyNoMoreInteractions(displayMock);
        verifyNoMoreInteractions(changeTrayMock);
    }
}
